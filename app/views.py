# -*- encoding: utf-8 -*-
"""
Copyright (c) 2019 - present AppSeed.us
"""
import pymysql
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template import loader
from django.http import HttpResponse
from django import template


def connection():
    conn = pymysql.connect(
        host='192.168.41.19',
        port=13306,
        user='sentiment',
        password='123456',
        db='sentiment',
        charset='utf8mb4',
        cursorclass=pymysql.cursors.DictCursor
    )
    return conn


@login_required(login_url="/")
def index(request):
    with connection().cursor() as connect:
        query_stock = f'''SELECT stock,name FROM sentiment.mxh_stocks;'''
        connect.execute(query_stock)
        result = connect.fetchall()
        stockname = []
        for x in result:
            stockname.append(x['stock'])

        query_industry = f'''SELECT DISTINCT industry FROM sentiment.mxh_stocks;'''
        connect.execute(query_industry)
        industry_resulting = connect.fetchall()
        industry = []
        for y in industry_resulting:
            industry.append(y['industry'])

    context = {
        'stockname': stockname,
        'industry': industry
    }
    context['segment'] = 'index'

    html_template = loader.get_template('dashboard.html')
    return HttpResponse(html_template.render(context, request))


@login_required(login_url="/")
def get_search_form(request):
    if request.method == "POST":
        stockname = request.POST.get('stockname')
        industryname = request.POST.get('industryname')

    with connection().cursor() as connect:

        # User nhập mã ngành
        if stockname is None:
            query_industry = f'''SELECT * 
                        FROM sentiment.mxh_news_by_industry 
                        RIGHT JOIN sentiment.mxh_stocks 
                        ON mxh_news_by_industry.stock = mxh_stocks.stock 
                        WHERE industry = '{industryname}'
                        ORDER BY ID DESC limit 45;'''
            connect.execute(query_industry)
            industry_result = connect.fetchall()

            query_count_industry = f'''SELECT COUNT(id)
                                     FROM sentiment.mxh_news_by_industry
                                     RIGHT JOIN sentiment.mxh_stocks
                                     ON mxh_news_by_industry.stock=mxh_stocks.stock
                                     WHERE industry = '{industryname}';'''
            connect.execute(query_count_industry)
            industry_count_result = connect.fetchall()
            total_industry_results = industry_count_result[0]['COUNT(id)']

            # Số bài viết Positive
            query_pos_industry = f'''SELECT COUNT(id)
                                     FROM sentiment.mxh_news_by_industry
                                     RIGHT JOIN sentiment.mxh_stocks
                                     ON mxh_news_by_industry.stock=mxh_stocks.stock
                                     WHERE industry = '{industryname}' 
                                     AND sentiment_tag = 'POS';'''
            connect.execute(query_pos_industry)
            pos_industry = connect.fetchall()
            pos_industry = pos_industry[0]['COUNT(id)']

            # Số bài viết Negative
            query_neg_industry = f'''SELECT COUNT(id)
                            FROM sentiment.mxh_news_by_industry
                            RIGHT JOIN sentiment.mxh_stocks
                            ON mxh_news_by_industry.stock=mxh_stocks.stock
                            where industry = '{industryname}' 
                            and sentiment_tag = 'NEG';'''
            connect.execute(query_neg_industry)
            neg_industry = connect.fetchall()
            neg_industry = neg_industry[0]['COUNT(id)']

            context_industry = {
                'industry_name': industryname,
                'results': industry_result,
                'industry_count_results': total_industry_results,
                'pos': pos_industry,
                'neg': neg_industry
            }
            context_industry['segment'] = 'industry'

            html_template = loader.get_template('results-page.html')
            return HttpResponse(html_template.render(context_industry, request))
        else:
            stockname = stockname.upper()
            # Số bài viết dựa theo mã cổ phiếu
            query_stock = f'''SELECT website_name, publishdate_at, original_images, titles, original_links, sentiment_tag  
                                    FROM sentiment.mxh_news_by_industry 
                                    WHERE stock = '{stockname}'
                                    ORDER BY ID DESC limit 45;'''
            connect.execute(query_stock)
            stock_result = connect.fetchall()

            # Đếm số bài viết dựa theo mã cổ phiếu
            query_count_stocks = f'''SELECT COUNT(id) FROM sentiment.mxh_news_by_industry WHERE stock = '{stockname}';'''
            connect.execute(query_count_stocks)
            stock_count_result = connect.fetchall()
            total_stock_results = stock_count_result[0]['COUNT(id)']

            # Số bài viết Positive
            query_pos = f'''SELECT COUNT(id) FROM sentiment.mxh_news_by_industry WHERE stock = '{stockname}' and sentiment_tag='POS';'''
            connect.execute(query_pos)
            pos_stock = connect.fetchall()
            pos_stock = pos_stock[0]['COUNT(id)']

            # Số bài viết Negative
            query_neg = f'''SELECT COUNT(id) FROM sentiment.mxh_news_by_industry WHERE stock = '{stockname}' and sentiment_tag='NEG';'''
            connect.execute(query_neg)
            neg_stock = connect.fetchall()
            neg_stock = neg_stock[0]['COUNT(id)']
            context_stock = {
                'stock_name': stockname,
                'stock_results': stock_result,
                'stock_count_results': total_stock_results,
                'pos': pos_stock,
                'neg': neg_stock
            }
            context_stock['segment'] = 'stock'
            html_template = loader.get_template('results-page.html')
            return HttpResponse(html_template.render(context_stock, request))


@login_required(login_url="/")
def pages(request):
    context = {}
    # All resource paths end in .html.
    # Pick out the html file name from the url. And load that template.
    try:

        load_template = request.path.split('/')[-1]
        context['segment'] = load_template
        html_template = loader.get_template(load_template)
        return HttpResponse(html_template.render(context, request))

    except template.TemplateDoesNotExist:

        html_template = loader.get_template('page-404.html')
        return HttpResponse(html_template.render(context, request))

    except:

        html_template = loader.get_template('page-500.html')
        return HttpResponse(html_template.render(context, request))
